raspi_0_options = -o target_arch arm -o target_machine raspi_0

build:
	bst $(raspi_0_options) build target/raspi-0w.bst

deployment: build
	bst $(raspi_0_options) checkout target/raspi-0w.bst deployment

clean:
	rm -rf deployment
